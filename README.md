# Symbiote Technical Test

Hello and welcome to this demo created by Sami Ur Rehman. This demo consists of two main UI components, the Dashboard on the left and Page on the right. In order to login, please make sure that username is the same as the password. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Clone the Repository

### `git clone git@bitbucket.org:sam_rehman_39/symbiote-technical-test.git`

### Change to the symbiote-technical-test Directory

### `cd symbiote-technical-test`

### Install the Dependencies

### `npm install`

## Run the Demo

### `npm start`

And that is it! You should now be able to view it in your browser.

## Built With

[React](https://reactjs.org)

[Redux](https://redux.js.org/)

Some common dependencies that I have made use of include:

[Formik](https://github.com/jaredpalmer/formik)

[Bootstrap](https://getbootstrap.com)

## Code Decisions

This demo could still be improved and is by no means perfect. I had some time constraints so I focused on some things more than the others. These are the things I would have done if I had more time:

- Made less use of bootstrap and more of SCSS to improve the overall look and feel of the application to provide a better user experience.

- Written unit tests. I understand that testing is really important.

- Worked on authentication process a little bit more.

- Played around a little more to make it more interactive and improve the overall user experience.

## Conclusion

Lastly, I hope you like this demo. I would love to get some feedback on what you think of it and how it can be improved :)
