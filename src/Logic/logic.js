export const authCheck = (username, password) => username === password;

export const pageCheck = title => {
  const pages = JSON.parse(localStorage.getItem("pages"));
  if (!pages) {
    return true;
  } else {
    const result = pages.filter(page => page.title === title).length === 0;
    return result;
  }
};
