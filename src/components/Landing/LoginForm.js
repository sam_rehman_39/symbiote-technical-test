import React from "react";
import { connect } from "react-redux";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { authenticateUser } from "../../actions/authActions";
import "../../styles/shared.scss";

const InnerForm = props => {
  const { errors, touched } = props;
  return (
    <Form>
      <div className="m-4 field">
        <Field
          className="form-control"
          type="text"
          name="username"
          placeholder="Username"
        />
        {touched.username && errors.username && (
          <span className="text-danger small error">{errors.username}</span>
        )}
      </div>
      <div className="m-4 field">
        <Field
          className="form-control"
          type="password"
          name="password"
          placeholder="Password"
        />
        {touched.password && errors.password && (
          <span className="text-danger small error">{errors.password}</span>
        )}
      </div>
      <div className="text-center">
        <button className="btn btn-primary" type="submit">
          Login
        </button>
      </div>
    </Form>
  );
};

const LoginForm = withFormik({
  mapPropsToValues: () => ({
    username: "",
    password: ""
  }),
  handleSubmit: ({ username, password }, { props: { authenticateUser } }) => {
    authenticateUser(username, password);
  },
  validationSchema: Yup.object().shape({
    username: Yup.string().required("Name is required"),
    password: Yup.string().required("Password is required")
  })
})(InnerForm);

export default connect(
  () => ({}),
  { authenticateUser }
)(LoginForm);
