import React, { Component } from "react";
import LoginForm from "./LoginForm";
import { connect } from "react-redux";
import { logout } from "../../actions/authActions";
import { clearPages } from "../../actions/pagesActions";

import "./landing.scss";
import "../../styles/shared.scss";

class Landing extends Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout() {
    const { logout, clearPages } = this.props;
    logout();
    clearPages();
  }

  render() {
    const { auth, isFetching, error, username } = this.props;
    if (isFetching) {
      return <div className="loader mx-auto" />;
    }
    return (
      <div className="landing-container mt-4">
        <div>
          <span className="h5 text-secondary">
            Hello and welcome to this demo created by{" "}
            <span className="font-weight-bold">Sami Ur Rehman</span>. This demo
            consists of two main UI components, the Dashboard on the left and
            the Page on the right. Please login below to get started.
          </span>
        </div>
        {!auth ? (
          <div>
            <LoginForm />
            {error && (
              <div className="m-3 text-center">
                <span className="text-danger">Authentication failed.</span>
              </div>
            )}
          </div>
        ) : (
          <div className="m-3 text-center">
            <div>
              <span className="text-success">
                You are currently logged in as "{username}".
              </span>
            </div>
            <button className="btn btn-danger mt-2" onClick={this.handleLogout}>
              Logout
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default connect(
  ({ auth: { auth, isFetching, error, username } }) => ({
    auth,
    isFetching,
    error,
    username
  }),
  { logout, clearPages }
)(Landing);
