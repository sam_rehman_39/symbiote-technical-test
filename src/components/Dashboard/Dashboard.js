import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getPages } from "../../actions/pagesActions";

import "../../styles/shared.scss";

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.renderLinks = this.renderLinks.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { auth, getPages } = this.props;
    if (!prevProps.auth && auth) {
      getPages();
    }
  }

  renderLinks() {
    const { pages } = this.props;
    if (!pages || pages.length === 0) {
      return null;
    } else {
      return pages.map(({ title }, idx) => {
        const location = { pathname: "/pages", state: { title } };
        return (
          <div className="mt-3" key={idx}>
            <Link to={location} className="mt-2 mt-md-3">
              {title}
            </Link>
          </div>
        );
      });
    }
  }

  render() {
    const { auth, isFetching } = this.props;
    return (
      <div className="container-fluid mt-3 mb-3">
        <h2>Dashboard</h2>
        <Link to="/" className="mt-2 mt-md-3">
          Home
        </Link>
        {this.renderLinks()}
        {auth && !isFetching && (
          <div className="mt-4">
            <Link to="/create" className="btn btn-primary">
              Add page
            </Link>
          </div>
        )}
      </div>
    );
  }
}

export default connect(
  ({ auth: { auth, isFetching }, pages: { pages } }) => ({
    auth,
    isFetching,
    pages
  }),
  { getPages }
)(Dashboard);
