import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { createPage } from "../../actions/pagesActions";
import "../../styles/shared.scss";

class InnerForm extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidMount() {
    const { auth, history } = this.props;
    if (!auth) {
      history.replace("/");
    }
  }

  componentDidUpdate(prevProps) {
    const {
      isFetching,
      err,
      values: { title },
      history
    } = this.props;
    if (prevProps.isFetching && !isFetching) {
      if (err) {
        this.setState(() => ({ error: "Page already exists." }));
      } else {
        this.setState(() => ({ error: null }));
        const location = { pathname: "/pages", state: { title } };
        history.push(location);
      }
    }
  }

  render() {
    const { errors, touched } = this.props;
    const { error } = this.state;
    return (
      <div className="container-fluid mt-3">
        <Form>
          <label className="h4">
            Create a new page here by adding a title and content:
          </label>
          <div className="m-4 field">
            <Field
              className="form-control"
              type="text"
              name="title"
              placeholder="Page title"
            />
            {touched.title && errors.title && (
              <span className="text-danger small error">{errors.title}</span>
            )}
          </div>
          <div className="m-4 field">
            <Field
              className="form-control"
              component="textarea"
              name="content"
              placeholder="Add page content here"
            />
          </div>
          <div className="text-center">
            <button className="btn btn-primary" type="submit">
              Create
            </button>
            <div className="m-3">
              <span className="text-danger">{error}</span>
            </div>
          </div>
        </Form>
      </div>
    );
  }
}

const CreatePage = withFormik({
  mapPropsToValues: () => ({
    title: "",
    content: ""
  }),
  handleSubmit: ({ title, content }, { props: { createPage } }) => {
    createPage(title, content);
  },
  validationSchema: Yup.object().shape({
    title: Yup.string().required("Title is required")
  })
})(InnerForm);

export default connect(
  ({ auth: { auth }, pages: { isFetching, error: err } }) => ({
    isFetching,
    err,
    auth
  }),
  { createPage }
)(withRouter(CreatePage));
