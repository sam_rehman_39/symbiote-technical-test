import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Header from "./Header/Header";
import Landing from "./Landing/Landing";
import CreatePage from "./CreatePage/CreatePage";
import Dashboard from "./Dashboard/Dashboard";
import Pages from "./Pages/Pages";

import "../styles/shared.scss";

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <div>
            <Header />
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-3 bg-light full-viewport-height">
                  <Dashboard />
                </div>
                <div className="col-md-9">
                  <Route exact path="/" component={Landing} />
                  <Route path="/create" component={CreatePage} />
                  <Route path="/pages" component={Pages} />
                </div>
              </div>
            </div>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
