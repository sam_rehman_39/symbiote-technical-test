import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import "../../styles/shared.scss";

const Pages = props => {
  const {
    location: { state },
    pages
  } = props;
  if (!pages || !state) {
    return null;
  }
  const page = pages.filter(page => page.title === state.title)[0];
  return (
    <div className="container-fluid mt-3">
      <h2>{page.title}</h2>
      <p>{page.content}</p>
    </div>
  );
};

export default connect(
  ({ pages: { pages } }) => ({ pages }),
  {}
)(withRouter(Pages));
