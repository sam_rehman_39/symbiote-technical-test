import pagesReducer from "./pagesReducer";
import authReducer from "./authReducer";
import { combineReducers } from "redux";

export default combineReducers({
  auth: authReducer,
  pages: pagesReducer
});
