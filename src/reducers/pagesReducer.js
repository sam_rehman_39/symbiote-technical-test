import {
  CREATING_PAGE,
  CREATE_PAGE_SUCCESS,
  CREATE_PAGE_ERROR,
  GET_PAGES,
  CLEAR_PAGES
} from "../actions/types";

const initialState = { isFetching: false, error: false, pages: [] };

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case CREATING_PAGE:
    case CREATE_PAGE_ERROR:
    case GET_PAGES:
    case CLEAR_PAGES:
      return { ...state, ...payload };
    case CREATE_PAGE_SUCCESS:
      const {
        isFetching,
        error,
        pages: { title, content }
      } = payload;
      if (state.pages.length > 0) {
        return {
          ...state,
          isFetching,
          error,
          pages: [...state.pages, { title, content }]
        };
      } else {
        return {
          ...state,
          isFetching,
          error,
          pages: [{ title, content }]
        };
      }
    default:
      return state;
  }
}
