import {
  AUTHENTICATING_USER,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR,
  LOGOUT
} from "../actions/types";

const initialState = {
  auth: false,
  isFetching: false,
  error: false,
  username: ""
};

export default function(state = initialState, { type, payload }) {
  switch (type) {
    case AUTHENTICATING_USER:
    case AUTHENTICATE_USER_ERROR:
    case AUTHENTICATE_USER_SUCCESS:
    case LOGOUT:
      return { ...state, ...payload };
    default:
      return state;
  }
}
