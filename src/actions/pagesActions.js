import {
  CREATING_PAGE,
  CREATE_PAGE_SUCCESS,
  CREATE_PAGE_ERROR,
  GET_PAGES,
  CLEAR_PAGES
} from "./types";
import { pageCheck } from "../Logic/logic";

export const createPage = (title, content) => async dispatch => {
  dispatch({
    type: CREATING_PAGE,
    payload: { isFetching: true, error: false }
  });
  if (pageCheck(title)) {
    const pages = JSON.parse(localStorage.getItem("pages"));
    localStorage.setItem(
      "pages",
      pages
        ? JSON.stringify([...pages, { title, content }])
        : JSON.stringify([{ title, content }])
    );
    dispatch({
      type: CREATE_PAGE_SUCCESS,
      payload: { isFetching: false, error: false, pages: { title, content } }
    });
  } else {
    dispatch({
      type: CREATE_PAGE_ERROR,
      payload: { isFetching: false, error: true }
    });
  }
};

export const getPages = () => async dispatch => {
  const pages = JSON.parse(localStorage.getItem("pages"));
  if (pages) {
    dispatch({ type: GET_PAGES, payload: { pages } });
  } else {
    dispatch({ type: GET_PAGES, payload: { pages: [] } });
  }
};

export const clearPages = () => async dispatch => {
  dispatch({ type: CLEAR_PAGES, payload: { pages: [] } });
};
