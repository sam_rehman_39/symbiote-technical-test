import {
  AUTHENTICATING_USER,
  AUTHENTICATE_USER_SUCCESS,
  AUTHENTICATE_USER_ERROR,
  LOGOUT
} from "./types";
import { authCheck } from "../Logic/logic";

export const authenticateUser = (username, password) => async dispatch => {
  dispatch({
    type: AUTHENTICATING_USER,
    payload: { auth: false, isFetching: true, error: false }
  });
  if (authCheck(username, password)) {
    dispatch({
      type: AUTHENTICATE_USER_SUCCESS,
      payload: { auth: true, isFetching: false, error: false, username }
    });
  } else {
    dispatch({
      type: AUTHENTICATE_USER_ERROR,
      payload: { auth: false, isFetching: false, error: true }
    });
  }
};

export const logout = () => async dispatch => {
  dispatch({
    type: LOGOUT,
    payload: { auth: false, isFetching: false, error: false, username: "" }
  });
};
